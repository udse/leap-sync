#ifndef LEAP_H
#define LEAP_H

#define SPORT 0x3f8

static DEFINE_SPINLOCK(sport_lock);

// level of RTS / DTR lines
static int level_RTS = 1;
static int level_DTR = 1;

// flag bit values for RTS / DTR lines
static u8 port_level[4] = { 0x00, 0x01, 0x02, 0x03 };

// Synchronize TSC and DAQ sample index
static void daq_sync(void)
{
        // Toggle serial line for re-sync of TSC and DAQ sample index
        level_RTS = (level_RTS + 1) % 2;
        level_DTR = (level_DTR + 1) % 2;

        // Toggle serial port pin i.e. generate sync signal
        spin_lock(&sport_lock);
        outb_p(port_level[2 * level_DTR + level_RTS], SPORT + 4);
        spin_unlock(&sport_lock);
}

#endif

