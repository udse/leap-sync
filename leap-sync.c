#include <linux/module.h>
#include <linux/kthread.h>
#include <linux/kprobes.h>
#include <linux/kallsyms.h>

#define CREATE_TRACE_POINTS

#include "leap-sync.h"

static struct kprobe kp;

static unsigned long jiffy_previous = 0;

int pre_do_timer(struct kprobe *p, struct pt_regs *regs)
{
	u64 tsc;

	if((jiffies - jiffy_previous) >= 25) {
		rdtscll(tsc);
		jiffy_previous = jiffies;
		trace_sync(tsc);
	}

	return 0;
}

static int __init leap_sync_init(void)
{
	int ret;
	
	kp.pre_handler = pre_do_timer;
	kp.addr = (kprobe_opcode_t *) kallsyms_lookup_name("do_timer");
	
	if (!kp.addr) {
		printk("Couldn't find %s to plant kprobe\n", "do_timer");
		return -1;
	}
	
	if ((ret = register_kprobe(&kp) < 0)) {
		printk("register_kprobe failed, returned %d\n", ret);
		return -1;
	}
	
	printk("kprobe registered\n");
	
	return 0;
}

static void __exit leap_sync_exit(void)
{
	unregister_kprobe(&kp);
	
	printk("kprobe unregistered\n");
}

module_init(leap_sync_init);
module_exit(leap_sync_exit);

MODULE_AUTHOR("James Clause");
MODULE_DESCRIPTION("leap-sync");
MODULE_LICENSE("GPL");

