#undef TRACE_SYSTEM
#define TRACE_SYSTEM leap

#if !defined(_LEAP_SYNC_H) || defined(TRACE_HEADER_MULTI_READ)
#define _LEAP_SYNC_H

#include "leap.h"

#include <linux/tracepoint.h>

TRACE_EVENT(sync, TP_PROTO(u64 tsc),
		  TP_ARGS(tsc),
		  TP_STRUCT__entry(
				     __field(u64, tsc)
		  ),
		  TP_fast_assign(
				     __entry->tsc = tsc;
				     daq_sync();
		  ),
		  TP_printk("tsc=%llu", __entry->tsc)
	   );

#endif /* _LEAP_SYNC_H */

/* This part must be outside protection */
#undef TRACE_INCLUDE_PATH
#define TRACE_INCLUDE_PATH .
#define TRACE_INCLUDE_FILE leap-sync
#include <trace/define_trace.h>

